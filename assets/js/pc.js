$(document).ready(function () {
 
  $('.bt-passou').on('click', 'a', function() {
    passou($(this).attr('data-id'));
  });

  $('.bt-correcao').on('click', 'a', function() {
    correcao($(this).attr('data-id'));
  });
    
    $('.bt-executou').on('click', 'a', function () {
        executou($(this).attr('data-id'));
    });    

	async function passou(dataId) {
		swal({
			title: 'Deseja confirmar a passagem?',
			type: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: $("#input-ajax-base-url").val() + 'passou',
					dataType: 'json',
					method: 'POST',
					data: { pcGrupoId: dataId },
					success: function (data) {
            location.reload();
						//window.location.href = '../';
					},
					error: function(jqxhr, status, exception) {
						alert(exception);
					}
				});
			}
		});
    }
    
    async function executou(dataId) {
        swal({
            title: 'Deseja confirmar a execução da atividade?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: $("#input-ajax-base-url").val() + 'atividade',
                    dataType: 'json',
                    method: 'POST',
                    data: { pcGrupoId: dataId },
                    success: function (data) {
                        location.reload();
                    },
                    error: function (jqxhr, status, exception) {
                        alert(exception);
                    }
                });
            }
        });
    }    
  
	async function correcao(dataId) {
		const {value: name} = swal({
			type: 'warning',
			title: 'Preencha o horário para a correção',
      showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			cancelButtonText: 'Cancelar',
			html: `<input id="swal-input1" class="swal2-input" type="time" step="1" autofocus>`,
			preConfirm: function () {
				return new Promise((resolve) => {
					if (!$("#swal-input1").val()) {
						swal.showValidationError('Preencha o horário corretamente!');
					}
					resolve();
				})
			}
		}).then((result) => {
			if (result.value) {
				$.ajax({
					url: $("#input-ajax-base-url").val() + 'correcao',
					dataType: 'json',
					method: 'POST',
					data: { pcGrupoId: dataId, horario: $("#swal-input1").val() },
					success: function (data) {
            location.reload();
					},
					error: function(jqxhr, status, exception) {
						alert(exception);
					}
				});
			}      
    });
  }

});
