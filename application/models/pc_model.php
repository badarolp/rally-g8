<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Pc_model extends CI_Model {

    function listaPCs() {
        $this->db->select('PCS.id, PCS.nome, PCS.ordem, PCS.tempo');
        $this->db->from('pcs as PCS');
        $this->db->order_by("ordem", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    function listaPCsByGrupo($grupo_numero) {
        $this->db->select('PCS.id, PCS.nome, PCS.ordem, PCS.tempo');
        $this->db->from('pcs as PCS');
        $this->db->where('PCS.grupo_numero', $grupo_numero);
        $this->db->order_by("ordem", "asc");
        return $query->result();
    }
    

    

/*
    function listaPCs() {
        $this->db->select('PCS.id, PCS.ordem, PCS.nome, PCS.tempo');
        $this->db->from('pcs as PCS');
        $this->db->order_by("ordem", "asc");
        $query = $this->db->get();
        return $query->result();
    }
*/    

}

  