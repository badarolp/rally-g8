<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users" aria-hidden="true"></i> Grupos
      <small>Gerenciamento de Grupos</small>
    </h1>
  </section>
    
  <section class="content">

    <div class="row">
      <?php
      if(!empty($listaGrupos)) {
        foreach($listaGrupos as $grupo) {
      ?>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-<?php echo $grupo->cor;?>">
          <div class="inner">
            <h3 style="font-size: 220%;">Grupo <?php echo $grupo->numero ?></h3>
            <br/>
            <br/>
          </div>
          <div class="icon">
            <?php echo $grupo->numero ?>
          </div>
          <a href="<?php echo base_url().'grupo/'.$grupo->numero ?>" class="small-box-footer">Abrir <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <?php
        }
      }
      ?>
    </div>

  </section>

</div>