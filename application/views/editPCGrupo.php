<div class="content-wrapper">
    <section class="content-header">
      <h1>
      <i class="fa fa-users" aria-hidden="true"></i> Grupo <?php echo $grupo_numero; ?>
      <small>Edição dos dados no PC <?php echo $pc_ordem; ?></small>
      </h1>
    </section>
    
    <section class="content">    
      <div class="row">
        <div class="box box-primary">

          <div class="box-header">
            <h3 class="box-title">Detalhes do Grupo no Ponto de Cronometragem</h3>
          </div>
                    
          <form role="form" action="<?php echo base_url(); ?>salvaPCGrupo" method="post" id="salvaPCGrupo" role="form">
            <div class="box-body">
              <div class="row">

                <div class="col-md-2">
                  <div class="form-group">
                    <label for="lg_passou">Passou</label><br/>
                    <input type="text" class="form-control" id="lg_passou" name="lg_passou" value="<?php echo $lg_passou; ?>">
                    <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $id; ?>">
                    <input type="hidden" class="form-control" id="grupo_numero" name="grupo_numero" value="<?php echo $grupo_numero; ?>">
                  </div>
                </div>

                <div class="col-md-2">                                
                  <div class="form-group">
                    <label for="passou">Passou às</label><br/>
                    <input type="text" class="form-control" id="passou" name="passou" value="<?php echo $passou; ?>">
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="form-group">
                    <label for="tempo">Tempo</label><br/>
                    <input type="text" class="form-control" id="tempo" name="tempo" value="<?php echo $tempo; ?>">
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="form-group">
                    <label for="lg_atividade">Executou atividade</label><br/>
                    <input type="text" class="form-control" id="lg_atividade" name="lg_atividade" value="<?php echo $lg_atividade; ?>">
                  </div>
                </div>                                

            </div>
            
            <div class="box-footer">
                <input type="submit" class="btn btn-success" value="Salvar"/>
                <a type="button" class="btn btn-danger" href="javascript:history.go(-1)">Cancelar</a>
            </div>

          </form>
        </div>

      </div>

    </section>
</div>