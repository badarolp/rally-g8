<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users" aria-hidden="true"></i> Grupo <?php echo $grupoNumero; ?>
      <small>Gerenciamento</small>
    </h1>
  </section>
  
  <section class="content">
  
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Lista de Pontos de Cronometragem</h3>
          </div><!-- /.box-header -->

          <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th class="text-left">Ponto de Cronometragem</th>
                <th class="text-center">Passou</th>
                <th class="text-center">Tempo</th>
                <th class="text-center">Passou às</th>
                <th class="text-center">Atividade</th>
                <th class="text-right">AÇÕES</th>
              </tr>

              <form role="form" action="#" method="post" id="editPCGrupos" role="form">
                <?php
                if(!empty($listaPcGrupos)) {
                  foreach($listaPcGrupos as $pcGrupo) {
                ?>
                <tr>
                  <td> PC <?php echo $pcGrupo->pc_ordem; ?></td>
                  <td class="text-center">
                    <?php
                    if($pcGrupo->lg_passou == 0) {
                      ?> <i class="fa fa-square-o"></i> <?php
                    } elseif($pcGrupo->lg_passou == 1) {
                      ?> <i class="fa fa-check-square"></i> <?php
                    }
                    ?>
                  </td>
                  <td class="text-center"><?php echo $pcGrupo->tempoGrupo; ?></td>
                  <td class="text-center"><?php echo $pcGrupo->passou; ?></td>
                  <td class="text-center">
                        <?php
                        if($pcGrupo->lg_atividade == 0) {
                        ?> <i class="fa fa-square-o"></i> <?php
                        } elseif($pcGrupo->lg_atividade == 1) {
                        ?> <i class="fa fa-check-square"></i> <?php
                        }
                        ?>
                  </td>                  
                  <td class="text-right">
                    <a class="btn btn-sm btn-warning" href="<?php echo base_url().'editpcgrupo/'.$pcGrupo->id; ?>"><i class="fa fa-pencil"></i></a>
                  </td>
                </tr>
                <?php
                  }
                }
                ?>
              </form>  

            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-2 pull-left">
        <a type="button" class="btn btn-warning btn-block" href="javascript:history.go(-1)"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
      </div>
    </div>

  </section>
</div>