<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-users" aria-hidden="true"></i> Relatórios
      <small>Relatórios do Resultado</small>
    </h1>
  </section>
  
  <section class="content">

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">PC 1</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <!-- /.col -->
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Largada <i class="fa fa-arrow-right"></i> Acarajé (Praça do Gil)</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Grupo 1</span>
                  <span class="progress-number"><b>-44</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-lilas" style="width: 78%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 2</span>
                  <span class="progress-number"><b>-38</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 81%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 3</span>
                  <span class="progress-number"><b>-4</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 98%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 4</span>
                  <span class="progress-number"><b>-98</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-laranja" style="width: 51%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 5</span>
                  <span class="progress-number"><b>-84</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 58%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->                  
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Tempo do PC</h5>
                  <span class="description-percentage text-green">00:06:50</span>
                  </br>
                  <span class="description-text"><i class="fa fa-flag"></i></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->            
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Maior tempo</h5>
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> 00:06:54</span>
                  </br>
                  <span class="description-text">Grupo 3</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">Menor tempo</h5>
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 00:06:01</span>
                  </br>
                  <span class="description-text">Grupo 4</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <h5 class="description-header">Melhor tempo</h5>
                  <span class="description-percentage text-blue">00:06:54</span>
                  <br>
                  <span class="description-text">Grupo 3</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">PC 2</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <!-- /.col -->
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Acarajé (Praça do Gil) <i class="fa fa-arrow-right"></i> Posto Aline (Vivaldo Mendes)</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Grupo 1</span>
                  <span class="progress-number"><b>-122</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-lilas" style="width: 39%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 2</span>
                  <span class="progress-number"><b>-18</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 91%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 3</span>
                  <span class="progress-number"><b>-23</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 88%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 4</span>
                  <span class="progress-number"><b>-92</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-laranja" style="width: 54%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 5</span>
                  <span class="progress-number"><b>-35</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 82%"></div> 
                  </div>
                </div>
                <!-- /.progress-group -->                  
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
      
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Tempo do PC</h5>
                  <span class="description-percentage text-green">00:04:43</span>
                  </br>
                  <span class="description-text"><i class="fa fa-flag"></i></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->            
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Maior tempo</h5>
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> 00:05:18</span>
                  </br>
                  <span class="description-text">Grupo 5</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">Menor tempo</h5>
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 00:03:42</span>
                  </br>
                  <span class="description-text">Grupo 1</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <h5 class="description-header">Melhor tempo</h5>
                  <span class="description-percentage text-blue">00:04:34</span>
                  <br>
                  <span class="description-text">Grupo 2</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->

        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">PC 3</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <!-- /.col -->
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Posto Aline (Vivaldo Mendes) <i class="fa fa-arrow-right"></i> Hiper</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Grupo 1</span>
                  <span class="progress-number"><b>-86</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-lilas" style="width: 57%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 2</span>
                  <span class="progress-number"><b>-69</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 65%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 3</span>
                  <span class="progress-number"><b>-49</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 75%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 4</span>
                  <span class="progress-number"><b>-58</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-laranja" style="width: 71%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 5</span>
                  <span class="progress-number"><b>-67</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 66%"></div> 
                  </div>
                </div>
                <!-- /.progress-group -->                  
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
      
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Tempo do PC</h5>
                  <span class="description-percentage text-green">00:04:01</span>
                  </br>
                  <span class="description-text"><i class="fa fa-flag"></i></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->            
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Maior tempo</h5>
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> 00:05:27</span>
                  </br>
                  <span class="description-text">Grupo 1</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">Menor tempo</h5>
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 00:04:50</span>
                  </br>
                  <span class="description-text">Grupo 3</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <h5 class="description-header">Melhor tempo</h5>
                  <span class="description-percentage text-blue">00:04:50</span>
                  <br>
                  <span class="description-text">Grupo 3</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->

        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">PC 4</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <!-- /.col -->
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Hiper <i class="fa fa-arrow-right"></i> Farmácia em frente a Caixa (Olívia Flores)</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Grupo 1</span>
                  <span class="progress-number"><b>-90</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-lilas" style="width: 55%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 2</span>
                  <span class="progress-number"><b>-20</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 90%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 3</span>
                  <span class="progress-number"><b>-90</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 55%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 4</span>
                  <span class="progress-number"><b>-130</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-laranja" style="width: 35%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 5</span>
                  <span class="progress-number"><b>-50</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 75%"></div> 
                  </div>
                </div>
                <!-- /.progress-group -->                  
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
      
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Tempo do PC</h5>
                  <span class="description-percentage text-green">00:09:05</span>
                  </br>
                  <span class="description-text"><i class="fa fa-flag"></i></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->            
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Maior tempo</h5>
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> 00:08:55</span>
                  </br>
                  <span class="description-text">Grupo 2</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">Menor tempo</h5>
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 00:08:00</span>
                  </br>
                  <span class="description-text">Grupo 4</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <h5 class="description-header">Melhor tempo</h5>
                  <span class="description-percentage text-blue">00:08:55</span>
                  <br>
                  <span class="description-text">Grupo 2</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->

        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">PC 5</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <!-- /.col -->
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Farmácia em frente a Caixa (Olívia Flores) <i class="fa fa-arrow-right"></i> Cachorro quente perto da AABB (Olívia Flores)</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Grupo 1</span>
                  <span class="progress-number"><b>-294</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-lilas" style="width: 3%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 2</span>
                  <span class="progress-number"><b>-104</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 48%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 3</span>
                  <span class="progress-number"><b>-26</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 87%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 4</span>
                  <span class="progress-number"><b>-302</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-laranja" style="width: 2%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 5</span>
                  <span class="progress-number"><b>-25</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 87%"></div> 
                  </div>
                </div>
                <!-- /.progress-group -->                  
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
      
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Tempo do PC</h5>
                  <span class="description-percentage text-green">00:04:55</span>
                  </br>
                  <span class="description-text"><i class="fa fa-flag"></i></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->            
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Maior tempo</h5>
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> 00:05:21</span>
                  </br>
                  <span class="description-text">Grupo 3</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">Menor tempo</h5>
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 00:02:24</span>
                  </br>
                  <span class="description-text">Grupo 4</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <h5 class="description-header">Melhor tempo</h5>
                  <span class="description-percentage text-blue">00:05:21</span>
                  <br>
                  <span class="description-text">Grupo 3</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->

        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">PC 6</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <!-- /.col -->
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Cachorro quente perto da AABB (Olívia Flores) <i class="fa fa-arrow-right"></i> Centro de Cultura</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Grupo 1</span>
                  <span class="progress-number"><b>-78</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-lilas" style="width: 61%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 2</span>
                  <span class="progress-number"><b>-31</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 84%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 3</span>
                  <span class="progress-number"><b>-119</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 40%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 4</span>
                  <span class="progress-number"><b>-121</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-laranja" style="width: 39%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 5</span>
                  <span class="progress-number"><b>-78</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 61%"></div> 
                  </div>
                </div>
                <!-- /.progress-group -->                  
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
      
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Tempo do PC</h5>
                  <span class="description-percentage text-green">00:07:31</span>
                  </br>
                  <span class="description-text"><i class="fa fa-flag"></i></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->            
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Maior tempo</h5>
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> 00:09:32</span>
                  </br>
                  <span class="description-text">Grupo 4</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">Menor tempo</h5>
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 00:08:02</span>
                  </br>
                  <span class="description-text">Grupo 2</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <h5 class="description-header">Melhor tempo</h5>
                  <span class="description-percentage text-blue">00:08:02/span>
                  <br>
                  <span class="description-text">Grupo 2</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->

        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>        

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">PC 7</h3>
            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
              </div>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
            <!-- /.col -->
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Centro de Cultura <i class="fa fa-arrow-right"></i> Nathfarma</strong>
                </p>
                <div class="progress-group">
                  <span class="progress-text">Grupo 1</span>
                  <span class="progress-number"><b>-102</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-lilas" style="width: 49%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 2</span>
                  <span class="progress-number"><b>-42</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-red" style="width: 79%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 3</span>
                  <span class="progress-number"><b>-10</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-aqua" style="width: 95%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 4</span>
                  <span class="progress-number"><b>-382</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-laranja" style="width: 1%"></div>
                  </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                  <span class="progress-text">Grupo 5</span>
                  <span class="progress-number"><b>-18</b>/0</span>
                  <div class="progress sm">
                    <div class="progress-bar progress-bar-yellow" style="width: 91%"></div> 
                  </div>
                </div>
                <!-- /.progress-group -->                  
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
      
          <div class="box-footer">
            <div class="row">
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Tempo do PC</h5>
                  <span class="description-percentage text-green">00:09:53</span>
                  </br>
                  <span class="description-text"><i class="fa fa-flag"></i></span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->            
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                  <h5 class="description-header">Maior tempo</h5>
                  <span class="description-percentage text-yellow"><i class="fa fa-caret-up"></i> 00:16:15</span>
                  </br>
                  <span class="description-text">Grupo 4</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block border-right">
                <h5 class="description-header">Menor tempo</h5>
                  <span class="description-percentage text-red"><i class="fa fa-caret-down"></i> 00:09:32</span>
                  </br>
                  <span class="description-text">Grupo 2</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-3 col-xs-6">
                <div class="description-block">
                  <h5 class="description-header">Melhor tempo</h5>
                  <span class="description-percentage text-blue">00:10:03</span>
                  <br>
                  <span class="description-text">Grupo 3</span>
                </div>
                <!-- /.description-block -->
              </div>
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-footer -->

        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>    

  </section>
</div>