<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <i class="fa fa-home" aria-hidden="true"></i> Home
      <small>Painel Principal</small>
    </h1>
  </section>
    
  <section class="content">

    <?php //echo phpinfo(); ?>

    <div class="row">
      <div class="col-md-12">
        <ul class="timeline">    

          <li>
            <i class="fa fa-check bg-green"></i>
            <div class="timeline-item">
              <h3 class="timeline-header"><b>Passando</b></h3>
              <div class="timeline-body">
                  <div class="timeline-body">
                  Ao passar pelo PC, o momento atual e o intervalo de tempo serão salvos.
                  O sistema irá subtrair 1 ponto para cada segundo atrasado, ou 2 pontos 
                  para cada segundo adiantado. Caso a equipe pule um PC, o sistema incluirá 
                  uma multa de 1200 pontos.
                </div>
              </div>
            </div>
          </li>
          <li>
            <i class="fa fa-flag bg-blue"></i>
            <div class="timeline-item">
              <h3 class="timeline-header"><b>Resultado</b></h3>
              <div class="timeline-body">
                  <div class="timeline-body">
                    O resultado será calculado automaticamente ao final do Rally. 
                    O grupo que tiver menos multas (ou seja o valor mais próximo de zero) 
                    será o vencedor.
                </div>
              </div>
            </div>            
          </li>
          <li>
            <i class="fa fa-clock-o bg-gray"></i>
          </li>  

        </ul>
      </div>

  </section>

</div>