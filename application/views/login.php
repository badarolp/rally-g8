<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>RallyG8 | Entrar</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url('assets/css/login.css') ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="login-page">

    <div class="main">
      <div class="container">
			  <center>
				  <div class="middle">
              
          <div class="row">
          <div class="logo">
                  <img alt="RallyG8" class="login_logo" src="<?php echo base_url('assets/images/logo2.png');?>">
                    <div class="clearfix"></div>
                  </div>
              </div>
              </div>


            <div class="row">
              
              <div id="login">

                <?php $this->load->helper('form'); ?>
                <div class="row">
                  <div class="col-md-12">
                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                  </div>
                </div>

                <?php
                $this->load->helper('form');
                $error = $this->session->flashdata('error');
                if($error) {
                ?>

                <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $error; ?>                    
                </div>

                <?php }
                $success = $this->session->flashdata('success');
                if($success) {
                ?>
                <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <?php echo $success; ?>                    
                </div>

                <?php } ?>

                <form action="<?php echo base_url(); ?>loginMe" method="post">  
                  <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Usuário" name="email" required />
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
                  
                  <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Senha" name="password" required />
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  
                  <div class="row">
                    <div class="col-xs-8 pull-right">
                      <input type="submit" class="btn btn-primary btn-block btn-flat" value="Entrar" />
                    </div>
                  </div>

                </form>

                <div class="clearfix"></div>

              </div>


				  </div>
			  </center>
		  </div>
	  </div>

    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

  </body>
</html>