<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Grupo (PCController)
 * Descrição.
 * @author : Lucas Badaró
 * @version : 0.1
 * @since : 18 Março 2018
 */
class Grupo extends BaseController {

    public function __construct() {        
        parent::__construct();
        $this->load->model('Grupo_model');
        $this->load->model('Pc_model');
        $this->load->model('Pc_grupo_model');
        $this->isLoggedIn();   
    }

    public function index() {
        $data['listaGrupos'] = $this->Grupo_model->listaGrupos();
        $this->global['pageTitle'] = 'RallyG8 : Grupos';
        $this->loadViews("grupos", $this->global, $data, NULL);
    }
    
    public function gerenciaGrupo($grupo_numero) {
        $data['listaPcGrupos'] = $this->Pc_grupo_model->listaPcGruposByGrupo($grupo_numero);
        $data['grupoNumero'] = $grupo_numero;
        $this->global['pageTitle'] = 'RallyG8 : Grupo '.$grupo_numero;
        $this->loadViews("grupo", $this->global, $data, NULL);
    }

    public function editpcgrupo($pcGrupoId) {
        $data = $this->Pc_grupo_model->getPcGrupo($pcGrupoId);
        $this->global['pageTitle'] = 'RallyG8 : Edição de Grupo/PC';
        $this->loadViews("editpcgrupo", $this->global, $data, NULL);
    }

    function salvaPCGrupo() {
        $id = $this->input->post('id');
        $grupo_numero = $this->input->post('grupo_numero');
        $lg_passou = $this->input->post('lg_passou');
        $lg_atividade = $this->input->post('lg_atividade');
        $passou = $this->input->post('passou');
        if(empty($passou)) {
            $passou = null;
        }
        $tempo = $this->input->post('tempo');
        if(empty($tempo)) {
            $tempo = null;
        }
        $pcGrupoInfo = array('lg_passou'=>$lg_passou, 'lg_atividade'=>$lg_atividade, 'tempo'=>$tempo);
        $this->Pc_grupo_model->update($pcGrupoInfo, $id);
        redirect('grupo/'.$grupo_numero);                
    }

}

?>