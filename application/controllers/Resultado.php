<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Resultado extends BaseController {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Pc_grupo_model');
        $this->load->model('Grupo_model');
        $this->isLoggedIn();
    }
    
    public function index() {
        $data['listaPcGruposGrouped'] = $this->Pc_grupo_model->listaPcGruposGrouped();
        $this->global['pageTitle'] = 'RallyG8 : Resultado';
        $this->loadViews("resultado", $this->global, $data, NULL);
    }

    public function calcular() {
        $listaGrupos = $this->Grupo_model->listaGrupos();
        foreach($listaGrupos as $grupo) {
            $listaPcGrupos = $this->Pc_grupo_model->listaPcGruposByGrupo($grupo->numero);
            $pontos = 0;
            foreach($listaPcGrupos as $pcGrupo) {
                $pontos = 0;
                if($pcGrupo->lg_passou) {
                    if($pcGrupo->tempoGrupo) {
                        $tempoPC = new DateTime($pcGrupo->tempoPC);
                        $tempoGrupo = new DateTime($pcGrupo->tempoGrupo);                
                        $diff = $tempoPC->diff($tempoGrupo);
                        if($diff->invert == 1) { //adiantado
                            $multiplica = 2;
                        } else { //adiantado
                            $multiplica = 1;
                        }
                        $pontos = $pontos - ((($diff->i * 60) + $diff->s) * $multiplica);
                    }
                } else {
                    $pontos = $pontos - 1200;
                }
                $pcGrupoInfo = array('pontos'=>$pontos);
                $this->Pc_grupo_model->update($pcGrupoInfo, $pcGrupo->id);                
            }
        }
        $listaPcGruposGrouped = $this->Pc_grupo_model->listaPcGruposGrouped();
        $posicao = 1;
        foreach($listaPcGruposGrouped as $pcGrupoGrouped) {
            $grupoInfo = array('posicao'=>$posicao);
            $this->Grupo_model->update($grupoInfo, $pcGrupoGrouped->grupo_numero); 
            $posicao++;
        }
        $data['listaPcGruposGrouped'] = $this->Pc_grupo_model->listaPcGruposGrouped();
        $this->global['pageTitle'] = 'RallyG8 : Resultado';
        $this->loadViews("resultado", $this->global, $data, NULL);
        
    }    



}

?>